namespace Fakir

open System

module PostMeta =
    type PostMeta =
        { Title : string
          Date : string
          Filename : string
          Categories : string list
          Html : string }
