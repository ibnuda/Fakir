namespace Fakir

open System

module Helpers =

    let defaultOption opt def =
        match opt with
            | Some thing -> thing
            | None -> def

    let defaultEmpty opt =
        defaultOption opt ""

    let strToBool str =
        match str with
            | "1" -> true
            |  _  -> false

    let appendHtml str =
        str + ".html"
