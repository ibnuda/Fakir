namespace Fakir

open System

open Fue.Compiler
open Fue.Data

module Rss =

    open Helpers
    open PostMeta

    let rssPost url post =
        let xml = """<item>
  <title>{{{title}}}</title>
  <link>{{{link filename}}}</link>
  <guid isPermaLink="false">{{{link filename}}}</guid>
  <description><![CDATA[{{{content}}}]]></description>
 </item>"""
        let link filename = url + "posts/" + (appendHtml filename)
        init
        |> add "title"    post.Title
        |> add "link"     link
        |> add "filename" post.Filename
        |> add "content"  post.Html
        |> fromText xml

    let rss title url posts =
        let xml = """<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
 <title>{{{title}}}</title>
 <link>{{{url}}}</link>
 <atom:link href="{{{url}}}feed.xml" rel="self" type="application/rss+xml"/>
 <description>{{{description}}}</description>
</channel>
</rss>
        """
        init
        |> add "title"       title
        |> add "url"         url
        |> add "description" (List.fold (fun a b -> a + "" + rssPost url b) "" posts)
        |> fromText xml
