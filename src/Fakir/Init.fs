namespace Fakir

open System
open System.IO

module Init =

    open DefaultFiles

    let directories =
        [ "assets"
          "pages"
          "posts"
          "template"
          "website" ]

    let nonEmptyError = "Should be inside of an empty directory."

    let defaultFiles =
        [ "config",                          defaultFiles "config"
          "header",                          defaultFiles "header"
          "footer",                          defaultFiles "footer"
          "posts/2017-10-02-hello-world.md", defaultFiles "post"
          "assets/style.css",                defaultFiles "css"
          "pages/contact.md",                defaultFiles "contact" ]

    let initBlog () =
        if Directory.GetDirectories(".").Length > 2 then
            printfn "%s" nonEmptyError
        else
            // create directory and files and shit like that.
            ()
