namespace Fakir

open System

open Fue.Compiler
open Fue.Data

module Html =

    open PostMeta
    open Helpers

    let listPost baseUrl meta =
        let html = """<li><a href="{{{base}}}posts/{{{appendHtml post}}}">{{{title}}}<time>({{{date}}})</time></a></li>"""
        init
        |> add "base"       baseUrl
        |> add "post"       meta.Filename
        |> add "appendHtml" appendHtml
        |> add "title"      meta.Title
        |> add "date"       meta.Date
        |> fromText html

    let listPosts baseurl metas =
        List.map (listPost baseurl) metas
        |> List.fold (fun a b -> a + "\n" + b) ""

    let categoryIndex baseUrl category metas =
        let html = """<article class="category"><header><h1>Category: {{{category}}}</h1></header><section><ul>{{{listPosts base posts}}}</ul></section></article>"""
        init
        |> add "category"  category
        |> add "listPosts" listPosts
        |> add "base"      baseUrl
        |> add "posts"     metas
        |> fromText html

    let categoryLink (baseUrl : string) (category : string) =
        let html = """<li><a href="{{{base}}}categories/{{{appendHtml category}}}">{{{category}}}</a></li>"""
        init
        |> add "base"       baseUrl
        |> add "appendHtml" appendHtml
        |> add "category"   category
        |> fromText html

    let categoryLinks baseUrl post =
        List.map (categoryLink baseUrl) post.Categories
        |> List.fold (fun a b -> a + "\n" + b) ""

    let postHeader baseUrl meta =
        let html = """<h1><a href="{{{base}}}posts/{{{appendHtml post}}}">{{{title}}}</a></h1><time>{{{date}}}</time>\n"""
        init
        |> add "base"       baseUrl
        |> add "appendHtml" appendHtml
        |> add "post"       meta.Filename
        |> add "title"      meta.Title
        |> add "date"       meta.Date
        |> fromText html

    let postFooter baseUrl post =
        let html = """<span class="filed">Filed under:</span><ul>{{{categoryLinks base post}}}</ul>\n"""
        init
        |> add "categoryLinks" categoryLinks
        |> add "base"          baseUrl
        |> add "post"          post
        |> fromText html

    let pageHeader title name =
        let html = """<h1>{{{title}}}</h1>"""
        init
        |> add "title" title
        |> fromText html

    let postHtml baseUrl excerpt post =
        let html = """
<article>
<header>{{{postHeader baseUrl post}}}</header>
<section>{{{content}}}</section>
<footer>{{{postFooter baseUrl post}}}</footer>
</article>
        """
        let htmlMore = """
<article>
<header>{{{postHeader baseUrl post}}}</header>
<section>{{{untilMore content}}}{{{moreLink post}}}</section>
<footer>{{{postFooter baseUrl post}}}</footer>
</article>
        """
        let content = post.Html.Replace("baseUrl", baseUrl)
        let untilMore (s : string) = s.Split([| "<!--more-->" |], StringSplitOptions.None) |> Array.head
        let moreLink post =
            init
            |> add "baseUrl"    baseUrl
            |> add "appendHtml" appendHtml
            |> add "post"       post.Filename
            |> fromText """<a class="readmore" href="{{{baseUrl}}}posts/{{{appendHtml post}}}">Read more</a>"""
        if post.Html.Contains("<!--more-->") && excerpt then
            init
            |> add "postHeader" postHeader
            |> add "baseUrl"    baseUrl
            |> add "post"       post
            |> add "untilMore"  untilMore
            |> add "content"    content
            |> add "moreLink"   moreLink
            |> add "postFooter" postFooter
            |> fromText htmlMore
        else
            init
            |> add "postHeader" postHeader
            |> add "baseUrl"    baseUrl
            |> add "post"       post
            |> add "content"    content
            |> add "postFooter" postFooter
            |> fromText html
